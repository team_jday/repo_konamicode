﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Kombo
{
    public string[] buttons;
    private int currentIndex = 0; 

    public float TimeBetweenPresses = 0.8f;
    private float LastPress;

    public Kombo(string[] butt)
    {
        buttons = butt;
    }

    
    public bool Check()
    {
        if (Time.time > LastPress + TimeBetweenPresses) currentIndex = 0;
        {
            if (currentIndex < buttons.Length)
            {
                if ((buttons[currentIndex] == "Up" && Input.GetKeyDown(KeyCode.UpArrow)) ||

                    (buttons[currentIndex] == "Down" && Input.GetKeyDown(KeyCode.DownArrow)) ||

                    (buttons[currentIndex] == "right" && Input.GetKeyDown(KeyCode.RightArrow)) ||

                    (buttons[currentIndex] == "Left" && Input.GetKeyDown(KeyCode.LeftArrow)) ||

                    (buttons[currentIndex] == "A" && Input.GetKeyDown(KeyCode.A)) ||

                    (buttons[currentIndex] == "B" && Input.GetKeyDown(KeyCode.B)))
                {
                    LastPress = Time.time;
                    currentIndex++;
                }

                if (currentIndex >= buttons.Length)
                {
                    currentIndex = 0;
                    return true;
                }
                else return false;
            }
        }

        return false;
    }
}

public class konamicode : MonoBehaviour
{
    public Kombo konamiCode;
    public string input1;
    public string input2;
    public string input3;
    public string input4;
    public string input5;

    void Start()
    {
        List<string> code = new List<string>();
        code.Add("Up");
        code.Add("Down");
        code.Add("Left");
        code.Add("Right");
        code.Add("A");
        code.Add("B");

        string input1 = code[Random.Range(0, code.Count)];

        string input2 = code[Random.Range(0, code.Count)];

        string input3 = code[Random.Range(0, code.Count)];

        string input4 = code[Random.Range(0, code.Count)];

        string input5 = code[Random.Range(0, code.Count)];

        Debug.Log(input1 + " " + input2 + " " + input3 + " " + input4 + " " + input5);

        konamiCode = new Kombo(new string[] { input1, input2, input3, input4, input5 });
    }

    
    void Update()
    {

        if (konamiCode.Check ())
        {
        SceneManager.LoadScene (2);
        }
    }
}