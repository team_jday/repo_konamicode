﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    float timeLeft = 60;


    public Text countur;  // public if you want to drag your text object in there manually
    


    void Start () {
        countur = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;
        countur.text ="Time Left : " + ((int) timeLeft).ToString();
        if (timeLeft < 0)
        {
            SceneManager.LoadScene(3);
        }
        
    }
}
