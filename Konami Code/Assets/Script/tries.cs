﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class tries : MonoBehaviour {

    


    public int triesleft = 25;

    public Text counter;  // public if you want to drag your text object in there manually
    int scoreCounter;

    void Start ()
    {

        counter = GetComponent<Text>();
    }
	
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) ||
            Input.GetKeyDown(KeyCode.DownArrow) ||
            Input.GetKeyDown(KeyCode.RightArrow) ||
            Input.GetKeyDown(KeyCode.LeftArrow) ||
            Input.GetKeyDown(KeyCode.A) ||
            Input.GetKeyDown(KeyCode.B)) 
        {
            triesleft--;
        }

        if(triesleft <= 0)
        {
            SceneManager.LoadScene(3);
        }
        counter.text = "Tries Left: " + triesleft.ToString();
    }
}
